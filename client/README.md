# 프로젝트 소개
애자일 프로세스 기반의 협업 툴 서비스 입니다.
소통과 협업을 효과적으로 도울 수 있는 애자일 방식의 협업 툴을 직접 설계하고 개발하며, 애자일 방법론을 깊이 있게 이해하고자 하였습니다.

# 개발 환경
### Language
JavaScript

### Environment & Framework
Node.js / Express.js

### Front-end
React / React Redux / CSS

### Database
MySQL / goorm IDE(서버 배포)

### 배포
AWS EC2 / docker / docker compose

### tools
Postman / VS CODE